WaveUp
======

WaveUp is an Android app that *wakes up* your screen when you *wave* over the proximity sensor. Just wave your over the proximity sensor - probably somewhere near where you put your ear to speak - and your screen will turn on.

It actually turns the display on when you uncover the proximity sensor. As a consequence, it will also turn on when you take your smartphone out of your pocket or purse.

As far as I've seen, it doesn't seem to have a big impact on battery life. I still need to check some more, nonetheless. And this might be different on other Android devices.

I plan on uploading it to Google Play and F-Droid, but it isn't really ready yet (I have only tested it on one device).

Miscellaneous notes
--------
At the beginning I was thinking on calling it *Jedi Hand App* because you can do a similar movement to turn on your display. In the end I cowardly refrained from it: I read some scary articles - damn you Internet! - about legal issues with *Star Wars* trademarks.

This is the first Android app I have ever written, so beware!

This is also my first small contribution to the open source world! Finally!

I would love if you would give me feedback of any kind or contribute in any way!

Thanks for reading!