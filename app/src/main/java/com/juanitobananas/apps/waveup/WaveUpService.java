/*
 * Copyright (c) 2016 Juan Garcia
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.juanitobananas.apps.waveup;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class WaveUpService extends Service {
    private static final String TAG = "WakeUpService";
    private ProximitySensorManager proximitySensorManager;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        proximitySensorManager.stop();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        proximitySensorManager = ProximitySensorManager.getInstance(getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        proximitySensorManager.start();
        return super.onStartCommand(intent, flags, startId);
    }
}
