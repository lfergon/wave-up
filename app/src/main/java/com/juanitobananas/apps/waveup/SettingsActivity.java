/*
 * Copyright (c) 2016 Juan Garcia
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.juanitobananas.apps.waveup;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "SettingsActivity";

    public static final String ENABLED = "pref_enable";
    public static final String WAVE_MODE = "pref_wave_mode";
    public static final String POCKET_MODE = "pref_pocket_mode";
    public static final String LOCK_SCREEN = "pref_lock_screen";

    private static Context applicationContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        if (!isLockScreenAdmin()) {
            askUserToEnableLockScreenAdmin();
        }
        updateEnabledPreferences();
        startService();
        registerPreferencesListener();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(ENABLED)) {;
            updateEnabledPreferences();
            startService();
        } else if (key.equals(LOCK_SCREEN)) {
            if (!isLockScreenAdmin() && isLockScreenEnabled()) {
                askUserToEnableLockScreenAdmin();
            }
        } else {
            if (!isWaveModeEnabled() && !isPocketModeEnabled()) {
                Toast.makeText(this, R.string.pocket_mode_and_wave_mode_disabled_toast, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void startService() {
        if (isServiceEnabled()) {
            Log.i(TAG, "Starting WaveUpService");
            startService(new Intent(this, WaveUpService.class));
            Toast.makeText(this, R.string.wave_up_service_started, Toast.LENGTH_SHORT).show();
        } else {
            Log.i(TAG, "Stopping WaveUpService");
            stopService(new Intent(this, WaveUpService.class));
            Toast.makeText(this, R.string.wave_up_service_stopped, Toast.LENGTH_SHORT).show();
        }
    }

    private void askUserToEnableLockScreenAdmin() {
        ComponentName lockScreenAdminComponentName = new ComponentName(getApplicationContext(), LockScreenAdminReceiver.class);
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, lockScreenAdminComponentName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, R.string.lock_admin_rights_explanation);
        startActivityForResult(intent, 1);
    }

    private void updateEnabledPreferences() {
        for (String key : getPropertyKeys()) {
            if (!key.equals(ENABLED)) {;
                findPreference(key).setEnabled(isServiceEnabled());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerPreferencesListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterPreferencesListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterPreferencesListener();
    }

    private void registerPreferencesListener() {
        getPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    private void unregisterPreferencesListener() {
        getPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    public boolean isServiceEnabled() {
        return getPreferences().getBoolean(ENABLED, false);
    }

    public boolean isWaveModeEnabled() {
        return getPreferences().getBoolean(WAVE_MODE, false);
    }

    public boolean isPocketModeEnabled() {
        return getPreferences().getBoolean(POCKET_MODE, false);
    }

    public boolean isLockScreenEnabled() {
        return getPreferences().getBoolean(LOCK_SCREEN, false);
    }

    public boolean isLockScreenAdmin() {
        ComponentName adminReceiver = new ComponentName(getApplicationContext(), LockScreenAdminReceiver.class);
        return getPolicyManager().isAdminActive(adminReceiver);
    }

    private DevicePolicyManager getPolicyManager() {
        return (DevicePolicyManager) getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
    }

    private List<String> getPropertyKeys() {
        List<String> propertyKeys = new ArrayList<>();
        propertyKeys.add(ENABLED);
        propertyKeys.add(WAVE_MODE);
        propertyKeys.add(POCKET_MODE);
        propertyKeys.add(LOCK_SCREEN);
        return propertyKeys;
    }
}
