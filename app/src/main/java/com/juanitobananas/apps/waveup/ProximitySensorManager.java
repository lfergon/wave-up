/*
 * Copyright (c) 2016 Juan Garcia
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.juanitobananas.apps.waveup;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;

public class ProximitySensorManager implements SensorEventListener {
    private static final String TAG = "ProximitySensorManager";

    private static final int NEAR = 0;
    private static final int FAR = 1;

    private static final long POCKET_THRESHOLD = 2000; // Time from which it is considered that the device is in a pocket
    private static final long MIN_TIME_BETWEEN_SCREEN_ON_AND_OFF = 1500;

    private static final long MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_ON = 0;
    private static final long MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_OFF = 1000;

    private SensorManager sensorManager;
    private Sensor proximitySensor;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    private int lastMeasure = FAR;
    private long lastMeasureTime = 0;

    private long lastTimeScreenOnOrOff = 0;

    Context context;

    private static volatile ProximitySensorManager instance;

    private Thread turnOffScreenThread;


    public static ProximitySensorManager getInstance(Context context) {
        if (instance == null ) {
            synchronized (ProximitySensorManager.class) {
                if (instance == null) {
                    instance = new ProximitySensorManager(context);
                }
            }
        }

        return instance;
    }

    private ProximitySensorManager(Context context) {
        this.context = context;
        start();
    }

    public final void start () {
        boolean enabled = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.ENABLED, false);
        if (enabled) {
            Log.i(TAG, "WaveUp is enabled. Starting it.");
            initProximitySensor();
            initWakeLock();
        } else {
            Log.i(TAG, "WaveUp is NOT enabled. I will not start it.");
        }
    }

    public final void stop () {
        Log.i(TAG, "stop(): Unregistering Listener");
        sensorManager.unregisterListener(this);
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        // If the sensor changes, there is possibly a thread waiting to turn off the screen. It needs to be interrupted.
        if (turnOffScreenThread != null) {
            turnOffScreenThread.interrupt();
        }

        int currentMeasure = event.values[0] == event.sensor.getMaximumRange() ? FAR : NEAR;
        long currentTime = System.currentTimeMillis();

        String currentMeasureString = event.values[0] == event.sensor.getMaximumRange() ? "FAR" : "NEAR";
        Log.i(TAG, "Proximity sensor changed: " + currentMeasureString + " (time: " + currentTime + ")");

        boolean waveModeEnabled = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.WAVE_MODE, false);
        boolean pocketModeEnabled = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.POCKET_MODE, false);

        long timeSinceLastScreenOnOrOff = currentTime - lastTimeScreenOnOrOff;
        long timeBetweenMeasures = currentTime - lastMeasureTime; // How long ago the sensor changed for the last time
        if (timeSinceLastScreenOnOrOff > MIN_TIME_BETWEEN_SCREEN_ON_AND_OFF) { // Don't do anything if it turned on or off 2 seconds ago
            if (lastMeasure == NEAR && currentMeasure == FAR
                    && timeBetweenMeasures > MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_ON) { // Turn on?
                if ((pocketModeEnabled && timeBetweenMeasures > POCKET_THRESHOLD) ||
                        (waveModeEnabled && timeBetweenMeasures < POCKET_THRESHOLD)) {
                    turnOnScreen();
                }
            } else if (lastMeasure == FAR && currentMeasure == NEAR) { // Turn off?
                boolean lockScreenEnabled = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.LOCK_SCREEN, false);
                if (lockScreenEnabled) {
                    turnOffScreenThread = turnOffScreenThread(MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_OFF);
                    turnOffScreenThread.start();
                }
            }
        } else {
            Log.d(TAG, "Time since last screen on/off: " + timeSinceLastScreenOnOrOff + ". Not doing anything");
        }
        lastMeasure = currentMeasure;
        lastMeasureTime = currentTime;
    }
    
    private Thread turnOffScreenThread(final long delay) {
        return new Thread() {
            @Override
            public void run() {
                if (powerManager.isScreenOn()) {
                    Log.d(TAG, "Creating a thread to turn off display if still covered in " + delay/1000 + " seconds");
                    try {
                        Thread.sleep(delay);
                        turnOffScreen();
                    } catch (InterruptedException e) {
                        Log.d(TAG, "Interrupted thread: Uncovered sensor too fast. Turning off screen cancelled.");
                    }
                }
            }
        };
    }

    private void turnOnScreen() {
        if (!powerManager.isScreenOn()) {
            lastTimeScreenOnOrOff = System.currentTimeMillis();
            Log.i(TAG, "Switched from NEAR to FAR. Turning screen on");
            wakeLock.acquire();
            wakeLock.release();
        }
    }

    private void turnOffScreen() {
        if (powerManager.isScreenOn()) {
            lastTimeScreenOnOrOff = System.currentTimeMillis();
            Log.i(TAG, "Switched from FAR to NEAR. Turning screen off");
            DevicePolicyManager policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
            policyManager.lockNow();
        }

    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    private void initProximitySensor() {
        Log.d(TAG, "Initializing proximity sensor");
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void initWakeLock() {
        powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
    }
}